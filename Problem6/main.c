#include <stdio.h>

int searchNumber(int *nums, int length, int target) {
    int ans = -1;
    int i = 0;
    while (i < length) {
        if (nums[i] == target) {
            ans = i;
            i = length;
        }
        else {
            i++;
        }
    }

    return ans;
}

int main() {
    int nums[] = {2,4,63,130,3,-223,-24,5,134,1000000,88};
    int length = sizeof(nums)/ sizeof(int);
    int target = 0;
    printf("Enter a number you want to search:\n");
    scanf("%d", &target);

    printf("Result: %d\n",searchNumber(nums, length, target));

    return 0;
}