#include <stdio.h>

void inputNumbers(int *nums, int length) {
    printf("Enter %d numbers:\n", length);
    for (int i=0; i<length; i++) {
        scanf("%d", &nums[i]);
    }
}
void displayNumbers(int *nums, int length) {
    for (int i=0; i<length; i++) {
        printf("%d ", nums[i]);
    }
    printf("\n");
}

int calcSum(int *nums, int length) {
    int ans = 0;
    for (int i=0; i<length; i++) {
        ans += nums[i];
    }

    return ans;
}

double calcAvg(int *nums, int length) {
    int sum = calcSum(nums, length);
    double avg = (double)sum/(double)length;

    return avg;
}

int searchMax(int *nums, int length) {
    int max = nums[0];
    for (int i=1; i<length; i++) {
        if (max<nums[i]) {
            max = nums[i];
        }
    }
    return max;
}

int searchMin(int *nums, int length) {
    int min = nums[0];
    for (int i=1; i<length; i++) {
        if (min>nums[i]) {
            min = nums[i];
        }
    }
    return min;
}

int main() {
    int count = 5;
    int nums[count];
    char command='n';

    inputNumbers(nums, count);

    while (command != 'q') {
        printf("--------------------------\n");
        printf("Printing the array [press 'p']\n");
        printf("Printing the sum of all numbers in the array [press ‘c’]\n");
        printf("Printing the average of the numbers in the array [press ‘v’]\n");
        printf("Printing the minimum number in the array [press ‘m’]\n");
        printf("Printing the maximum number in the array [press ‘x’]\n");
        printf("Exit and terminate [press ‘q’]\n");
        printf("Enter a new array [press ‘n’] (with the same length)\n");
        printf("--------------------------\n");
        printf("Please choose menu:\n");
        scanf("%*c%c", &command);

        switch (command) {
            case 'p':
                displayNumbers(nums, count);
                break;
            case 'c':
                printf("Sum = %d\n", calcSum(nums, count));
                break;
            case 'v':
                printf("Avg = %.2f\n", calcAvg(nums, count));
                break;
            case 'm':
                printf("Min = %d\n", searchMin(nums, count));
                break;
            case 'x':
                printf("Max = %d\n", searchMax(nums, count));
                break;
            case 'n':
                inputNumbers(nums, count);
                break;
            default:
                break;
        }
    }

    return 0;
}