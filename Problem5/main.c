#include <stdio.h>

void printOddEven(int *a, int length) {
    int odd = 0, even = 0;
    for (int i=0; i<length; i++) {
        if (a[i]%2 == 0) {
            even++;
        } else {
            odd++;
        }
    }
    printf("The number of odd is %d\n", odd);
    printf("The number of even is %d\n", even);
}

int main() {
    int nums[] = {2, 5, 7, 2000, 33, -22, 10023, 341};
    int length = sizeof(nums)/ sizeof(int);
    printOddEven(nums, length);

    return 0;
}