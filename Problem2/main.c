#include <stdio.h>
#include <string.h>

void displayOriginalOrder(char *text, int length) {
    for (int i=0; i<length; i++) {
        printf("%c ", text[i]);
    }
    printf("\n");
}

void displayReverseOrder(char *text, int length) {
    for (int i=length-1; i>=0; i--) {
        printf("%c ", text[i]);
    }
    printf("\n");
}

int main() {
    int n = 5;
    char str[n];
    printf("Input %d characters (Enter:input is done):\n", n);
    fgets(str, n+1, stdin);

    if (strlen(str)<n) {
        printf("Inputted characters are too short.\n");
        return 1;
    }

    displayOriginalOrder(str, n);
    displayReverseOrder(str, n);

    return 0;
}