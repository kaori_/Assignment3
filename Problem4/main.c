#include <stdio.h>

float findMax(float *nums, int length) {
    float max = nums[0];

    for (int i=1; i<length; i++) {
        printf("%f\n", nums[i]);
        if (max < nums[i]) {
            max = nums[i];
        }
    }
    return max;
}

float findMin(float *nums, int length) {
    float min = nums[0];

    for (int i=1; i<length; i++) {
        if (min > nums[i]) {
            min = nums[i];
        }
    }
    return min;
}

int main() {
    float a[] = {3.3, -4.5, 67.3, 0.000002, 1000000.2};
    int length = sizeof(a)/ sizeof(float);
    float max, min = 0;
    printf("a = %f\n", a[4]);
    max = findMax(a, length);
    min = findMin(a, length);

    double diff = (double)(max - min);

    printf("Max - Min = %f\n", diff);

    return 0;
}