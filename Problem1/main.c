#include <stdio.h>

void displayNumbers(int *nums, int length) {
    for(int i=0; i<length; i++) {
        printf("%d ",nums[i]);
    }
    printf("\n");

}

int main() {
    int count = 5;
    int numbers[count];
    printf("Input %d numbers:\n", count);
    for (int i=0; i<count; i++) {
        scanf("%d", &numbers[i]);
    }
    displayNumbers(numbers, count);

    return 0;
}