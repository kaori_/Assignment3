#include <stdio.h>
#include <stdlib.h>

int *copyIntArray(int *a, int length) {
    int *copy;
    copy = malloc(sizeof(int)*length);

    for (int i=0; i<length; i++) {
        copy[i] = a[i];
    }
    return copy;
}

int main() {
    int a[] = {31,2,3,4,5};
    int length = sizeof(a)/ sizeof(int);
    int *b;
    b = copyIntArray(a, length);

    for (int i=0; i<length; i++) {
        printf("%d ", b[i]);
    }
    printf("\n");

    return 0;
}